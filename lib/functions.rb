require 'securerandom'
require 'fileutils'
require 'uri'
require 'digest'

module Cliz
  ##
  # Gets a folder path for the shortened url
  def get_a_folder_path output_directory, short_suggestion = nil
    folder_path = nil

    short_suggestion ||= generate_short_suggestion output_directory
    folder_path =  File.join(output_directory, short_suggestion, '/')

    if File.directory?(folder_path)
      raise ("This short URL is already taken.")
    end

    folder_path
  end

  def generate_short_suggestion output_directory
    counter = 0
    random_string = nil

    loop do
      random_string = SecureRandom.alphanumeric[..counter]
      folder_path =  File.join(output_directory, random_string, '/')
      break unless File.directory?(folder_path)
      counter += 1
    end

    random_string
  end

  def create_index_file url, folder_path
    html = %Q{
    <!DOCTYPE html>
    <html>
      <head>
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <script type="text/javascript">
          window.location.replace("https://cliz.in");
        </script>
        <meta http-equiv="refresh" content="0; url='https://cliz.in'" />
      </head>
      <body>
        <p>Please follow <a href="https://cliz.in">this link</a>.</p>
      </body>
    </html>
    }

    redirect_content = html.gsub 'https://cliz.in', url
    FileUtils.mkdir_p(folder_path)

    File.open "#{folder_path}/index.html", 'w' do |f|
      f.puts redirect_content
    end
  end

  def short config, url, short_suggestion = nil
    output_dir = config['output_dir']
    base_domain = config['base_domain']
    deploy_script = config['deploy_script']
    pre_script = config['pre_script']

    system("sh #{pre_script}") if pre_script

    short_suggestion ||= generate_short_suggestion output_dir
    folder_path = get_a_folder_path output_dir, short_suggestion
    create_index_file(url, folder_path)

    short_url = File.join base_domain, short_suggestion
    reverse_index config, short_url

    system("sh #{deploy_script}") if deploy_script
    
    short_url
  end

  ##
  # Gets the path of short_url
  # Say its https://cliz.in/abc/d, it returns abc/d
  def path config, short_url
    base_domain = config['base_domain']
    short_url.gsub base_domain, ""
  end

  def long_url config, short_url
    output_dir = config['output_dir']

    path = path config, short_url
    file_contents = File.open(File.join(output_dir, path, 'index.html')).readlines.join '\n'
    URI.extract(file_contents).first
  end

  def reverse_index config, short_url
    output_dir = config['output_dir']

    long_url = long_url config, short_url

    reverse_index_file = File.join output_dir, 'reverse-index', "#{Digest::SHA2.hexdigest long_url}"


    File.open reverse_index_file, "a" do |f|
      f.puts path(config, short_url)
    end
  end

  def previously_shortened_urls(config, url)
    output_dir = config['output_dir']

    reverse_index_file = File.join output_dir, 'reverse-index', "#{Digest::SHA2.hexdigest url}"

    if File.exist? reverse_index_file
      urls = []
      base_domain = config['base_domain']
      paths = File.open(reverse_index_file).readlines

      for path in paths
        urls << File.join(base_domain, path)
      end
    end

    urls
  end

  def print_urls(urls)
    for url in urls
      puts url
    end
  end
end
Gem::Specification.new do |s|
  s.name        = 'cliz'
  s.version     = '0.1.2'
  s.date        = '2021-10-28'
  s.summary     = "Cliz - The static URL shortner"
  s.description = "Cliz - The static URL shortner"
  s.authors     = ["Karthikeyan A K"]
  s.email       = 'mindaslab@protonmail.com'
  s.files       = ["lib/functions.rb"]
  s.executables << 'cliz'
  s.homepage    = 'https://cliz.in'
  s.license     = 'GPLV3'
end
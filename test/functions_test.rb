require 'test/unit'
require_relative '../lib/functions.rb'
require 'fileutils'
require 'yaml'

include Cliz

class FunctionsTest < Test::Unit::TestCase
  @@folder = './tmp'

  def test_get_a_folder_path_should_return_a_folder_path
    folder_path = get_a_folder_path(@@folder)
    assert_not_nil folder_path
  end

  def test_get_a_folder_path_shold_return_a_folder_path_when_short_suggestion_is_given
    short_suggestion = 'abc'
    expected_folder_path = "#{@@folder}/#{short_suggestion}/"
    FileUtils.rm_rf(expected_folder_path)
    folder_path = get_a_folder_path(@@folder, short_suggestion)
    assert_equal expected_folder_path, folder_path
  end

  def test_get_a_folder_path_shold_raise_exception
    short_suggestion = 'abc'
    folder_path = "#{@@folder}/#{short_suggestion}/"
    FileUtils.rm_rf folder_path
    FileUtils.mkdir folder_path

    assert_raise do
      get_a_folder_path(@@folder, short_suggestion)
    end

    FileUtils.rm_rf folder_path
  end

  def test_generate_short_suggestion
    assert_not_nil generate_short_suggestion(@@folder)
  end

  def test_create_index_file
    short_suggestion = 'abc'
    folder_path = "#{@@folder}/#{short_suggestion}/"
    url = 'https://123.com'

    create_index_file(url, folder_path)

    content = File.open(File.join(folder_path, 'index.html')).readlines.join "\n"

    assert_not_nil content.match(url)

    FileUtils.rm_rf folder_path
  end

  def test_short_should_contain_base_domain
    config = YAML.load(File.read('config.yml'))
    short_url = short config, 'https://mindaslab.github.io'
    assert_match config['base_domain'], short_url
  end

  def test_short_url_should_be_longer_than_base_domain_url
    config = YAML.load(File.read('config.yml'))
    short_url = short config, 'https://mindaslab.github.io'
    assert config['base_domain'].length <  short_url.length
  end

  def test_short_url_should_contain_short_suggestion
    short_suggestion = 'abc'
    folder_path = "#{@@folder}/#{short_suggestion}/"
    FileUtils.rm_rf folder_path
    config = YAML.load(File.read('config.yml'))
    short_url = short config, 'https://mindaslab.github.io', short_suggestion
    assert_match short_suggestion, short_url
  end
end
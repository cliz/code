# Cliz

Cliz is an abbreviation for Clisus. Clisus is abbreviation for Commandline static URL shortner. Commandline static URL shortner is an abbreviation for Commandline static uniform resource locator shortner.

To know more about URL shortner, checkout <https://cliz.in/2>.

## Why Cliz?

I am fan of Free Software <https://fsf.org> and I like to share links with my friends, but I do not want link shortner to track them, so I developed cliz.

# License

Cliz is licensed under [GPLV3](https://www.gnu.org/licenses/gpl-3.0.en.html).

# Getting Started

## Have Ruby Installed

Get Ryby Installed. For more checkout https://ruby-lang.org

## Install Cliz

```
gem install cliz
```

That's it!

## Configuration

Create a folder where you want to run cliz, have a config file named `config.yml` as shown

```
output_dir: '../cliz_output/'
base_domain: 'https://cliz.in/'
deploy_script: 'deploy.sh'
pre_script: 'pre.sh'
```

Where `output_dir` is the directory where you want the cliz to generate static files.

`base_domain` is the domain name where you deploy those static files so that if your friends visit `https://base_domain.com/cat`, they get redirected to a URL about your cat.

If you have a deploy script to deploy your site after cliz has generated your short URL, give its nae here. This is my deploy script:

```
{
  cd ../cliz_output
  git add -A
  git commit -am "a new url added"
  git push origin master
  cd -
} &> /dev/null
```

I use gitlab pages to host <https://cliz.in>. I have a `.gitlab-ci` file in my output directory which reads like this:

```
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - /
  only:
  - master
```

That builds my pages when deployed to master. The path of deploy script file should be specified in `deploy_script`. If you want to do something before cliz springs into action, like pulling your static files from the deploy repo, you can write a shell script for it and put it's path in `pre_script`. The content of my prescript looks as shown:

```
git pull origin master
```

## Using cliz

Use it like this in the directory that contains `config.yml`

```
$ cliz https://rubygems.org/gems/cliz
```

or you want a custom shortened url

```
$ cliz http://llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch.co.uk/ small
```

you should get an output like `https://base_domain.com/small` that should take you to <http://llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch.co.uk/>.

# Todo

Find todo list here <https://gitlab.com/cliz/code/-/issues>

# Contribute

There is room for tremendous improvement. Contributions welcome.

